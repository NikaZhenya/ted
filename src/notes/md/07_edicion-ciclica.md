<section epub:type="chapter" role="doc-chapter">

# Edición cíclica como proceso tradicional de publicación

El cuidado sobre el texto, el tipo de formato y las técnicas aplicadas
para la producción de un libro han variado durante siglos. Sin embargo, la
concepción metodológica ha permanecido casi invariable. Con la publicación
digital es posible dilucidarla no como *el* proceso, sino como *un* método
para la publicación de una obra que tal vez ya no es el más pertinente para las
necesidades actuales…

![Esquema de la metdología de edición cíclica como extensión de la edición tradicional.](../img/s07_img01.jpg)

## Recursos

1. «[Edición cíclica y edición ramificada: de la antesala a la “revolución” digital (1/3)](http://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-de-la-antesala-a-la-revolucion-digital/)».

----

## Edición «tradicional»

La edición «tradicional» ha empleado métodos análogos para la 
producción de obras la cual tiene las siguientes características:

1. Sin importar la técnica empleada tiene ciertos procesos comunes.
2. La reversibilidad es una cuestión problemática.
3. A pesar de su antigüedad, pervive una concepción metodológica en común.

### Procesos comunes

Desde los amanuenses hasta la imprenta la producción de una obra se 
percibe como la consecución paso a paso de estos procesos:

1. Edición y corrección.
2. Diseño y maquetación.
3. Cotejo y revisión.
4. Impresión.
6. Distribución.
7. Comercialización y administración.

En algunos casos unos procesos se dan al unísono u otro son omitidos.
Sin embargo, su orden en el proceso de producción se sostiene.

### Reversibilidad

Cada uno de estos procesos procede lentamente para evitar la mayor
cantidad de errores. ¿Por qué? La producción a través de medios análogos hace
muy difícil u oneroso el retorno a procesos previos para la corrección de 
errores.

La reversibilidad en algunas tecnologías es posible, pero no por ello es menos
problemática por la cuestión de tiempos y de costos que puede llegar implicar.

Es en este sentido que en la edición «tradicional» existe poca flexibilidad
en la reversibilidad de procesos, haciendo que cada uno siga su propio ritmo o,
como sucede en varios casos, se castigue la calidad editorial a favor de una
mayor velocidad.

### Concepción metodológica

Aunque parezca una perogrullada: los procesos en la edición 
«tradicional» se conciben como una progresión **unilateral** hasta la 
publicación y comercialización de una obra.

¿Qué de malo tiene la **unilateralidad** en la edición? Ninguna… siempre y
cuando el objetivo sea la producción de un solo formato.

## Edición «cíclica»

Con la llegada de la edición digital vino también el surgimiento
de nuevos formatos, también conocidos como «soportes», para las obras. El
multiformato deja de tener un uso minoritario a volverse casi una necesidad
en la edición actual.

### Reversibilidad revisitada

Uno de los principales ejes que permitió la introducción de la
tecnología digital dentro de los procesos editoriales fue la solución de un
antiguo problema: la reversibilidad.

Con el uso de *software* se hizo posible «modernizar» la escritura y la edición
al permitir no solo el ahorro considerable de espacio y de papel, sino también
al tornar *casi* de manera imperceptible las peripecias y constantes retornos
dentro de la redacción de textos y su posterior publicación.

La reversibilidad se tornó una ventaja: el CTRL + Z ha supuesto un cambio que
antes de la «revolución» digital había quedado como una característica deseada.

La reversibilidad se ha introducido a lo largo y ancho de todos los procesos
editoriales que no solo está presente en la redacción, la edición o el diseño,
sino también en la impresión. La impresión digital, a diferencia del *offset*,
permite la sustitución de páginas específicas de una obra en lugar de una
sustitución completa de pliegos o librillos.

### Supuesto desdeño al *ebook*

Así como la edición digital trajo consigo el uso positivo de la
reversibilidad, también ocasionó una opinión negativa del formato digital por
parte de varios editores que crecieron con formatos y métodos análogos.

Se dice que el *ebook* tiene al menos una de las siguientes carencias:

* Implica el dominio de una técnica y una postura ante el texto ajena a los
  procesos tradicionales: la informática y la percepción estructural de una
  obra antes que su capa gráfica.
* Se trata de un formato que «compite» con el impreso.
* Su calidad es inferior al impreso, por lo que lo más sensato es la cautela
  ante la introducción de estos nuevos formatos.
* La producción de más formatos es proporcional al encarecimiento en la 
  producción de una obra.

Ante esto es posible estar de acuerdo con lo siguiente:

* Es cierto que implica abordar el texto de otra manera, en donde el editor
  que surgió de un contexto análogo lo entenderá con dificultad.
* Ciertamente la publicación digital puede inducir a que el lector prefiere este 
  formato, por lo general más económico, que el impreso.
* En general la publicación digital tiene menor calidad *en relación con* los
  estándares de calidad de un impreso.
* La producción paso a paso implica un aumento en los tiempos y costos por
  la introducción de más formatos.

Pero esto también ayuda a visibilizar lo siguiente:

* El uso de las nuevas tecnologías de la información y la comunicación no es ya
  una novedad; de hecho, dentro del ámbito editorial la edición digital es desde
  hace tiempo el principal medio de tratamiento del texto: el problema no es de
  «adaptación» —acrítica o no— tecnológica, sino de «empoderamiento»; sin
  control sobre la técnica no puede existir un control sobre la calidad en la
  edición.
* En la actualidad las ventas de libros electrónicos no representa más del 3%
  de las ventas totales de libros en Latinoamérica; de darse el caso de un
  mayor consumo, es posible que —como sucede en Estados Unidos o la Unión
  Europea— el *ebook* catapulte la venta de impresos: los formatos no compiten,
  se complementan al apuntar a distintos gustos en la lectura.
* La calidad del libro electrónico ha ido mejorando a lo largo de los años, pero
  su mejora depende fundamentalmente en qué tanto el editor se involucre en el
  proceso: si la calidad del *ebook* sigue reglas ortotipográficas anglosajonas
  o germanas es porque los editores de habla hispana poco o nada han querido
  sumarse al esfuerzo de mejorar el formato; el desarrollador de *software*
  difícilmente podrá afinar detalles por su falta de conocimientos editoriales.
* La proporción entre cantidad de formatos y encarecimiento de una obra solo
  se da en un contexto metodológico específico: la edición «cíclica».

### Continuidad metodológica

Al surgir nuevos formatos y al convertirse la publicación multiformato
en una prioridad, la primera solución encontrada fue la siguiente:

* Repítase paso a paso cada uno de los procesos para cada formato.
* Búsquese soluciones para evadir la mayor cantidad de pasos posibles.

Lo que se obtiene con esta perspectiva metodológica es una continuidad
en los procesos tradicionales de edición: una solución *ad hoc* basada en la
repetición monótona o las soluciones «milagro».

En cada uno de los ciclos se hereda el trabajo realizado en el formato anterior
para volver a rediseñarlo, cotejarlo, publicarlo, distribuirlo y 
comercializarlo. La ventaja es que el editor no tiene necesidad de repensar su
trabajo. Pero la desventajas son las siguientes:

* La disminución en la calidad editorial en cada nuevo formato.
* La pérdida de control técnico en la edición digital.
* El aumento en los tiempos de producción.
* El encarecimiento de la obra.

Ante estos problemas, algunos editores han buscado la aplicación no controlado
de procesos de automatización mediante los conversores, esa clase de *software*
que milagrosamente convierte de un formato **final** a otro. Sin embargo, la
desilusión viene rápidamente cuando se percibe que la prontitud tiene un costo
muy alto: la total pérdida de control.

¿Qué solución nos queda? Quizá repensar el modo de producción y buscar un nuevo
método…

</section>

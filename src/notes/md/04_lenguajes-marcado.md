<section epub:type="chapter" role="doc-chapter">

# Lenguajes de marcado más comunes en la edición

El primer acercamiento que se tendrá para el cuidado y formateo del
texto será mediante lenguajes de marcado. Sin embargo, ¿qué son los lenguajes
de marcado? ¿Cómo se usan? ¿Qué posibilidades tenemos?

![Logotipo de Markdown.](../img/s04_img01.jpg)

## Recursos

1. [*What is EPUB 3?*](https://github.com/ColectivoPerroTriste/Tapancazo/raw/master/taller/What_Is_EPUB_3_.epub).
2. [«The ultimate guide to Markdown»](https://blog.ghost.org/markdown/).
3. [«Guía para Markdown en PDF»](https://github.com/NikaZhenya/cursitos/raw/master/modulos/practicos/P001-Markdown/src/pdf/markdown-cheatsheet-online.pdf).

----

## Lenguajes de marcado

Existen varios tipos de lenguajes de marcado: los ligeros, los 
pesados y los pensados como notación de objetos para el intercambio de datos.

### Markdown

El lenguaje de marcado ligero más popular es, sin dudas, Markdown, el cual
es:

* Un lenguaje abierto.
* Un lenguaje que permite estructurar el contenido e indicar ciertas pautas de diseño.
* Un lenguaje cuyos archivos tienen la extensión `.md`.
* Un lenguaje con una sintaxis que es:
  1. Simple de escribir.
  2. Fácil de leer.
  3. Entendible para las computadoras.

Ventajas: 

* Frente a otros lenguajes de marcado, como [HTML]{.versalita}, [XML]{.versalita}  o TeX, es rápido, legible e intuitivo.
* Frente a texto procesado, como Word, es ligero y abierto, pudiéndose utilizar cualquier programa para su escritura.
* Fluidez en la escritura.
* Brinda estructura, no solo apariencia.
* Posibilidad de conversión a otros lenguajes y formatos.

Desventajas:

  * Debido a que usa un enfoque WYSIWYM («lo que ves es lo que quieres decir»),
    no es común en el ámbito editorial.
  * Requiere el aprendizaje de algo nuevo; a saber, su sintaxis.

```
Formato | Tipo    | Programa usual  | Licencia
MD      | Abierto | Editor de texto | Variable
```

Ejemplo:

```markdown
# Encabezado 1

Esto es un párrafo con una *itálica*.

## Encabezado 2

> Bloque de cita con una **negrita**.
```

### Familias TeX y HTML

De lenguajes de marcado pesado tenemos la familia de TeX y la familia
[HTML]{.versalita}.

```
Formato | Tipo    | Programa usual  | Licencia
TeX     | Abierto | Editor de texto | Variable
HTML    | Abierto | Editor de texto | Variable
```

Ejemplo de TeX:

```tex
\begin{document}

\chapter{Encabezado 1}

Esto es un párrafo con una \textit{itálica}.

\section{Encabezado 2}

\begin{quote}
  Bloque de cita con una \textbf{negrita}.
\end{quote}

\end{document}
```

![Entorno de trabajo con TeX con TeXstudio.](../img/s04_img02.jpg)

Ejemplo de [HTML]{.versalita}:

```html
<!DOCTYPE html>
<html>
  <body>
    <h1>Encabezado 1</h1>
    <p>Esto es un párrafo con una <em>itálica</em>.</p>
    <h2>Encabezado 2</h2>
    <blockquote>Bloque de cita con una 
      <strong>negrita</strong>.</blockquote>
  </body>
</html>
```

### XML, JSON y YAML

Cuando se requiere un manejo de datos para archivos de consulta o
generación dinámica de contenido, la notación de objetos es la opción más
pertinente.

Es el [XML]{.versalita} el formato más dominante, pero también el de más difícil
lectura y escritura al formar parte de la familia [HTML]{.versalita}.

Ejemplo de [XML]{.versalita}:

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<publication>
  <content>
    <h1>Encabezado 1</h1>
    <p>Esto es un párrafo con una <em>itálica</em>.</p>
    <h2>Encabezado 2</h2>
    <blockquote>Bloque de cita con una 
      <strong>negrita</strong>.</blockquote>
  </content>
</publication>
```

Ante esta dificultad, existe una notación más ligera para los mismos fines que
es [JSON]{.versalita}.

Ejemplo de [JSON]{.versalita}:

```json
{
  "publication" : {
    "content" : {
      "h1": "Encabezado 1",
      "p": "Esto es un párrafo con una <em>itálica</em>.",
      "h2": "Encabezado 2",
      "blockquote": "Cita con <strong>negrita</strong>."
    }
  }
}
```

Sin embargo, como es perceptible, la estructura de un archivo [JSON]{.versalita}
puede ser muy confusa, por lo que [YAML]{.versalita} surge como una solución
a este problema.

Ejemplo de [YAML]{.versalita}:

```yaml
---
publication:
  content:
    h1: "Encabezado 1"
    p: "Esto es un párrafo con una <em>itálica</em>."
    h2: "Encabezado 2"
    blockquote: "Cita con <strong>negrita</strong>."
```

```
Formato | Tipo    | Programa usual  | Licencia
XML     | Abierto | Editor de texto | Variable
JSON    | Abierto | Editor de texto | Variable
YAML    | Abierto | Editor de texto | Variable
```

### Editores de texto

Los lenguajes de marcado más populares son formatos abiertos, esto permite que
puedan ser redactados desde cualquier tiepo de editor de textos, privados,
abiertos o libres. Los editores recomendados para poder escribir cualquiera
de estos formatos son:

```
Programa     | Licencia    | Plataforma
Geany        | Libre       | Multiplataforma
Gedit        | Libre       | Multiplataforma
Atom         | Libre       | Multiplataforma
Brackets     | Libre       | Multiplataforma
Sublime Text | Propietaria | Multiplataforma
Dreamweaver  | Propietaria | Windows y macOS
```

## Familia EPUB

![Las tres capas de una publicación electrónica.](../img/s04_img03.jpg)

Con el fin de tener un formato específico para la lectura a través de una pantalla,
que a su vez fuese estandarizado y abierto, surge el [EPUB]{.versalita}.

¿Qué es un [EPUB]{.versalita}?

1. Es un conjunto de archivos [XHTML]{.versalita} comprimidos para su portabilidad.
2. Incluye metadatos y tabla de contenidos.
3. Permite una lectura «líquida» del contenido.
4. Es un formato abierto y estandarizado.
5. Es el formato base para otros pares privativos como [AZW]{.versalita} o [IBOOKS]{.versalita}.
6. Fue desarrollado por International Digital Publishing Forum ([IDPF]{.versalita}).
7. Surge a partir del Open eBook Publication Structure ([OEBPS]{.versalita}).
8. La primera versión, la 2.0, fue liberada en septiembre del 2007.
9. En enero del 2017 [IDPF]{.versalita} pasó a formar parte del World Wide Web Consortium, al mismo tiempo que liberó la versión 3.1 del [EPUB]{.versalita}; sin embargo, la mayoría de los distribuidores solo aceptan la versión 3.0.0.

Pese a que el [EPUB]{.versalita} es el formato estándar, diversas corporaciones han creado
sus propios formatos cerrados a partir del [EPUB]{.versalita} u [OEBPS]{.versalita}, como:

* Amazon.
  * [AZW]{.versalita}. Formato que contempla dos versiones:
    1. [KF7]{.versalita}. Basado en el formato desarrollado por Mobipocket (`.mobi`), que a su vez se basó en el [OEBPS]{.versalita}, cuya extensión es `.azw`.
	2. [KF8]{.versalita}. Basado en el [EPUB3]{.versalita} cuya extensión es `.azw3`.
* iTunes.
  * [IBOOKS]{.versalita}. Basado en el [EPUB3]{.versalita} cuya extensión es `.ibooks`.

> La decisión de un formato cerrado puede ser por 1) intereses comerciales, 2)
> optimización en los dispositivos, 3) adición de [DRM]{.versalita} o 4) implementación
> de características no estandarizadas.

```
Formato | Tipo        | Programa usual | Licencia
EPUB    | Abierto     | Renderizador   | Variable
IBOOKS  | Propietario | iBooks         | Propietaria
MOBI    | Propietario | Familia Kindle | Propietaria
AZW3    | Propietario | Familia Kindle | Propietaria
```

## Estilos Markdown de Pecas

La sintaxis básica de Markdown o la extendida por [Pandoc](http://pandoc.org/)
carecen de varios elementos necesarios para el quehacer editorial. Esto se debe
a que Markdown nació para la escritura rápida de *blogs*, no para la creación
de publicaciones digitales.

Gracias a la herramienta de base que se utilizará para la creación de libros
—Pecas— es posible añadir estilos adicionales que modificarán el archivo
automáticamente después de haber sido convertido con Pandoc. Los estilos son
los siguientes.

### Etiquetas

En diseño, los cambios más relevantes en las etiquetas son:

* `body`. Tiene márgenes alrededor de 4-5 em.
* `h1` a `h3`. Tienen un tamaño mayor de fuente, alineados a la izquierda, sin división silábica y en serifa.
* `h4`. En itálica y negrita.
* `h5`. En negrita.
* `h6`. En itálica.
* `p`. Si al párrafo le sigue otro párrafo, el segundo tendrá una sangría de 1.5em.
* `a`. Sin decoración y en color gris.
* `img`. Tiene un tamaño al 100% de la caja.

### Clases

Lo más destacado de la hoja de estilos por defecto es la posibilidad de usar
diversas clases comunes a un libro:

* `justificado`. Justifica el texto; por defecto el texto es justificado, excepto en los encabezados.
* `derecha`. Alinea el texto a la derecha.
* `izquierda`. Alinea el texto a la izquierda.
* `centrado`. Centra el texto.
* `frances`. Genera un párrafo con sangría francesa.
* `sangria`. Fuerza una sangría.
* `sin-sangria`. Evita una sangría.
* `sin-separacion`. Evita la separación silábica; por defecto el texto tiene separación silábica, excepto en los encabezados.
* `invisible`. Invisibiliza un contenido, aunque respeta su espacio en el contenido.
* `oculto`. Oculta un contenido, no abarca espacio en el contenido.
* `bloque`. Despliega una etiqueta como bloque.
* `capitular`. Añade una letra capitular.
* `versal`. Muestra el texto en mayúsculas.
* `redonda`. Fuerza texto en redondas.
* `versalita`. Muestra el texto en versalitas.
* `li-manual`. Permite un listado con elementos manuales.
* `epigrafe`. Muestra un texto como epígrafe.
* `espacio-arriba1`. Añade una línea de separación.
* `espacio-arriba2`. Añade dos líneas de separación.
* `espacio-arriba3`. Añade tres líneas de separación.

Existen más clases, pero por el momento son menos relevantes.

### Uso en Markdown

Con [`pc-pandog`](https://github.com/ColectivoPerroTriste/Herramientas/tree/master/Archivo-madre/1-Pandog)
—la herramienta de conversión de Pecas cuya base es Pandoc— es posible agregar 
estilos de párrafo al colocar al final de cada párrafo unas llaves con los 
estilos o identificadores deseados. Por ejemplo, este Markdown:

```markdown
Esto es un párrafo que continúa 
aquí y se quiere a la derecha. {.derecha}

Este es otro párrafo al que se le
añaden dos clases, *un espacio arriba*
y **centrado**. {.espacio-arriba1 .centrado}

Pero también es posible añadir
identificadores y clases, como
*una sangría francesa* que se
identifique como `p01`. {.frances #p01}
```

Generará este HTML si se usa `pc-pandog`:

```html
<p class="derecha">Esto es un párrafo que continúa aquí 
  y se quiere a la derecha.</p>
<p class="centrado espacio-arriba1">Este es otro 
  párrafo al que se le añaden dos clases, <em>un 
  espacio arriba</em> y <bold>centrado</bold>.</p>
<p id="p01" class="frances">Pero también es posible 
  añadir identificadores y clases, como <em>una sangría 
  francesa</em> que se identifique como 
  <code>p01</code>.</p>
```

Parece una nimiedad, pero con esta posibilidad ¡ya es posible asignar estilos
de párrafo!

</section>

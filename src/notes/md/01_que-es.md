<section epub:type="chapter" role="doc-chapter">

# ¿Qué son la edición digital y las publicaciones digitales?

Cuando se habla del trabajo editorial en el contexto digital, uno de los
primeros problemas que se afrontan es la falta de un panorama general
sobre aquello que se llama «edición digital».

![Los dos estados de todo editor.](../img/s01_img01.jpg)

## Recursos

1. «[Edición digital como edición desde cero](http://marianaeguaras.com/edicion-digital-como-edicion-desde-cero/)».
2. «[Appbooks y realidad aumentada, ¡no todo son ebooks!](http://marianaeguaras.com/appbooks-y-realidad-aumentada-no-todo-son-ebooks/)».
3. [*Track Changes: A literary history of word processing*](https://github.com/NikaZhenya/cursitos/raw/master/modulos/teoricos/T000-Ques/src/epub/track-changes.epub).

----

## Edición y publicación digital

![Panorama de la edición digital.](../img/s01_img02.jpg)

La edición digital **no** es lo mismo que la publicación digital.

La edición digital:

* Es **un proceso** de publicación mediante *software* y *hardware*.
* Puede ser tanto para un formato impreso como uno digital.
* Surge de la posibilidad de mostrar texto en una pantalla.

La publicación digital:

* Es **un formato** leído a través de una pantalla.
* Se presenta en una gran variedad de tipos de archivos digitales.
* Nace después de la edición digital, como un deseo o necesidad de no utilizar material impreso.

> La popularidad de la publicación digital ha ayudado a visibilizar una
> serie de inconvenientes que se vienen arrastrando en los procesos editoriales.

## Tipos de edición digital

De manera general se puede indicar que existen dos tipos de edición digital
cuyos objetivos, técnicas, métodos y formatos son distintos.

### Edición digital estándar

* Objetivos:
	1. Apegarse a la experiencia de lectura de un impreso.
	2. Dar continuidad a los procesos tradicionales de edición.
* Técnica: con lenguajes de marcado o con herramientas gráficas.
* Método: edición tradicional, edición cíclica o edición ramificada.
* Formatos: [EPUB]{.versalita}, [PDF]{.versalita}, [HTML]{.versalita}, [XML]{.versalita}, Markdown, TeX, [CBR]{.versalita}, [MOBI]{.versalita}, etc.


### Edición digital no estandarizada

* Objetivos:
	1. Experimentar con otras experiencias de lectura.
	2. Producir publicaciones mediante procesos ajenos a la tradición.
* Técnica: principalmente con lenguajes de programación y de marcado.
* Método: metodologías para el desarrollo de *software* o videojuegos.
* Formatos: [EXE]{.versalita}, [DMG]{.versalita}, [APK]{.versalita}, [IPA]{.versalita}; paquetes instalables o archivos ejecutables.

## Procesos en la edición estándar

![Procesos editoriales con herramientas digitales.](../img/s01_img03.jpg)

En general es posible hablar de cinco procesos, donde existen tres principales:

* **Primer paso**. Va del proceso de redacción al de las primeras correcciones antes de vaciarlo a un formato determinado.
* **Impreso**. Arranca de las primeras correciones a la distribución o comercialización de formatos impresos.
* **Digital**. A partir de las primeras correcciones se produce y distribuye una publicación digital.

Los otros dos procesos secundarios, no son menos importantes que los 
anteriores, sino que se trata de actividades que no implican directamente la 
producción de una publicación:

* **Digitalización**. Esto proceso implica partir de un libro impreso para terminar con un texto digital.
* **Liberación**. También llamado «violación de derechos de autor», es la actividad de eliminar los candados digitales a una publicación electrónica.

</section>

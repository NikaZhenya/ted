<section epub:type="chapter" role="doc-chapter">

# Edición no estandarizada: aplicaciones y más

Las posibilidades tecnológicas actuales permiten una clase de 
publicaciones cuya metodología poco tiene que ver con la tradición editorial. 
Este tipo de productos editoriales es lo que últimamente se conoce como 
*appbooks*.

![Entorno de trabajo de Godot Engine.](../img/s10_img01.jpg)

## Recursos

1. «[Appbooks y realidad aumentada. ¡No todo son ebooks!](https://marianaeguaras.com/appbooks-y-realidad-aumentada-no-todo-son-ebooks/)» 

----

## Algunas características generales

|										|																						|
|---------------------------------------|---------------------------------------------------------------------------------------|
| ¿Cómo se abre?						| No requiere de un programa adicional para su ejecución.								|
| ¿Cómo se lee?							| La experiencia de lectura juega con las posibilidades que ofrece el contexto digital.	|
| ¿Cuánto pesa?							| El peso tiende a ser mayor a 30 [MB]{.versalita}.										|
| ¿A qué es afín?						| Muy afín a libros con mucho diseño o con contenidos dinámicos.						|
| ¿Cuáles funcionalidades tiene?		| Las funcionalidades se limitan al *hardware* del dispositivo.							|
| ¿Cómo es su producción?				| La producción es más afín al desarrollo de *software* o de videojuegos.				|
| ¿Cuáles tecnologías están implicadas?	| Se produce con variados tipos de tecnologías.											|
| ¿Cuál es el estándar?					| No existe un estándar para su creación.												|
| ¿Cuál es su formato?					| El formato es multiplataforma como paquete instalable o archivo ejecutable.			|

## Tipos de *appbooks*

No existe un programa especialmente diseñado para su creación. En su 
lugar, se utilizan una serie de diferentes tecnologías.

> [Lista en YouTube con algunos ejemplos](https://www.youtube.com/playlist?list=PLqfNe0w1_scIpNUjgmbRjPfDOUugSGz-a).

### HTML estático

* Tecnologías implicadas: [HTML]{.versalita}, [CSS]{.versalita} y JavaScript.
* Tipo de narrativa: hipertextual, animaciones simples y formularios.
* Ventajas: tipo de *appbook* más sencillo de hacer, adecuado para obras breves 
  o que no pretenden mucha interactividad.
* Desventajas: falta de optimización si se pretende que haga más del tipo de
  narrativa para la que es más apta; uso de servidor (solo si se desconoce cómo
  administrarlo).
* Modo de uso: acceso a internet a una página *web*.
* Ejemplo: [CLIteratura](http://cliteratu.re/).

### *Web apps*

* Tecnologías implicadas: [HTML]{.versalita}, [CSS]{.versalita}, JavaScript, 
  base de datos (MySQL o MongoDB) y lenguaje de programación del lado del 
  servidor ([PHP]{.versalita} o Ruby).
* Tipo de narrativa: manejo de grandes volúmenes de información (acervos, 
  bibliotecas o diccionarios), consultas e intercambio de información con el
  usuario.
* Ventajas: práctica omnipresencia y mantenimiento relativamente bajo.
* Desventajas: si no existe el formato adecuado, la organización de la 
  información será un calvario; necesidad de conocimiento de desarrollo *web*
  intermedio.
* Modo de uso: acceso a internet a una página *web*.
* Ejemplo: [*Stanford Encyclopedia of Philosophy*](https://plato.stanford.edu/).

### Aplicaciones híbridas

* Tecnologías implicadas: [HTML]{.versalita}, [CSS]{.versalita}, JavaScript y
  Cordova.
* Tipo de narrativa: narrativas textocéntricas, animaciones simples, 
  formularios, uso de *hardware* del dispositivo móvil.
* Ventajas: manera más sencilla de crear aplicaciones móviles.
* Desventajas: falta de optimización según dispositivos determinados, en algunos
  casos requiere configuración o programación nativa.
* Modo de uso: aplicaciones para celulares o tabletas.
* Ejemplo: [*Jazz at Lincoln Center*](https://itunes.apple.com/mx/app/jalc/id1007748257?mt=8) 
  o [*Un Siglo Fiel*](https://play.google.com/store/apps/details?id=com.nievedechamoy.atlas).

### Aplicaciones nativas

* Tecnologías implicadas: desarrollo nativo (Xcode para iOS o Android Studio 
  para Android).
* Tipo de narrativa: uso fuerte de animaciones e interacción del usuario, uso
  recurrente del *hardware* del dispositivo.
* Ventajas: alto grado de optimización, gran desempeño para aplicaciones con
  muchas funcionalidades.
* Desventajas: requiere conocimientos avanzados en programación; en algunos 
  casos requiere desarrollos paralelos.
* Modo de uso: aplicaciones para celulares o tabletas.
* Ejemplo: [*Our Choice*](https://itunes.apple.com/us/app/al-gore-our-choice-plan-to/id432753658?mt=8) 
  o [*Guía del Prado*](http://www.laguiadelprado.com/index.html).

### Aplicaciones con motor de videojuegos

* Tecnologías implicadas: motor de videojuegos (Godoy Engine, Unity 3D, Unreal Engine, 
  Cocos2d).
* Tipo de narrativa: alta jugabilidad, mucho contenido gráfico y animaciones.
* Ventajas: automáticamente traduce a código nativo por lo que no hay fuertes
  problemas de desempeño, además de que las interfaces suelen ser muy completas
  o amigables.
* Desventajas: requiere conocimientos de programación, ilustración digital,
  modelaje 3D y animación, no hay mucho control sobre el formato del texto y
  puede requerir varias horas de optimización si se desea salida a muchos
  dispositivos.
* Modo de uso: aplicaciones para móviles, aplicaciones para computadoras o
  consolas de videojuegos.
* Ejemplo: [Trabajo de Concretoons](http://cartuchera.concretoons.com/index.html), 
  [*The Mammoth*](https://inbetweengames.itch.io/mammoth-download), 
  [*En busca de Kayla*](https://play.google.com/store/apps/details?id=com.nievedechamoy.kayla)
  o [desarrolladores independientes en itch.io](https://itch.io/).

### Realidad aumentada y realidad virtual

* Tecnologías implicadas: motores de videojuegos y [SDK propietarios]{.versalita}.
* Tipo de narrativa: alta jugabilidad e interacción con entornos reales o 
  virtuales.
* Ventajas: ala más vanguardista de los *appbooks* y campo abierto para la
  experimentación entre la edición, el videojuego y nuevas tecnologías.
* Desventajas: los proyectos pueden a llegar a ser muy complejos, resultando en
  pobres soluciones si no existe el tiempo o la disposición adecuada, además
  de que implica la inversión en equipo o licencias de *software*.
* Modo de uso: aplicaciones para móviles, aplicaciones para computadoras o
  consolas de videojuegos.
* Ejemplo: [*La leyenda del ladrón*](http://www.pitboxmedia.com/la-leyenda-del-ladron-libro-con-realidad-aumentada/),
  [*UNAM Futuro*](https://itunes.apple.com/mx/app/unam-futuro/id1042823048?mt=8)
  o [*Moriarty*](http://store.steampowered.com/app/611050/Moriarty_Endgame_VR/).

## Posibles limitaciones

1. Los descuidos técnicos es lo primero que nota el usuario.
2. Necesidad de amplia planificación y gestión de recursos.
3. Para los profesionales de la edición, el desarrollo de *appbooks* requiere
   metodologías ajenas, como las de desarrollo de *software* o de videojuegos.
4. Falta de estandarización.
5. Uso frecuente de formatos y *software* propietario.
6. Necesidad de equipos interdisciplinarios.
7. Conocimientos al menos intermedios en programación.
8. Manejo de diversas jergas.
9. Aprendizaje de comercialización de *software*.
10. Continuo mantenimiento.
11. Familiarización con la cultura de los videojuegos.

## Su lugar hoy en día

* Los *appbooks* no han llegado a reemplazar a los *ebooks* o a los impresos.
* Excepción en el mundo editorial.
* Desarrolla más accesible para los desarrolladores de videojuegos, no para los
  editores.
* Experimentación y aprendizaje de nuevas experiencias de lectura.
* Uso publicitario.
* No pensarlo como un posible *bestseller* ni *longseller*.

</section>

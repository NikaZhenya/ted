<section epub:type="chapter" role="doc-chapter">

# Formatos y programas para la edición digital

Se cuenta con dos tendencias generales para el tratamiento del texto,
las cuales no solo tienen un distinto origen, sino también una diferente opinión
en torno al *software*. A lo largo de las décadas han existido formatos y
programas que han seguido algunas de estas dos líneas. A continuación veremos
algunos de estos.

![Línea de tiempo de los formatos más comunes de texto.](../img/s03_img01.jpg)

## Recursos

1. «[Adobe, el omnipresente](http://marianaeguaras.com/adobe-el-omnipresente/)».
2. [Artículo en la Wikipedia sobre «Formato abierto»](https://es.wikipedia.org/wiki/Formato_abierto). 

----

## Formatos y programas más utilizados

Del archivo original a los distintos formatos de publicación existen
una serie de formatos, varios de ellos intermedios, y de programas que nos
permiten ir de un extremo al otro de los procesos editoriales.

### Procesadores de texto

Por lo general, el proceso de producción de una obra comienza con un
archivo de procesador de texto dado por el autor. Su ventaja es que son los
formatos más populares, por lo que cualquier persona está familiarizada. Sin
embargo, la principal desventaja es su general falta de uniformidad y de 
estructura por lo que uno de los primeros trabajos no solo es corregir el texto,
sino también darle una estructura adecuada.

```
Formato | Tipo    | Programa usual | Licencia
DOCX    | Abierto | Word           | Propietario
ODT     | Abierto | Writer         | Libre
```

![Entorno de trabajo de LibreOffice Writer.](../img/s03_img02.jpg)

### *Desktop Publishing*

![Entorno de trabajo de Scribus.](../img/s03_img03.jpg)

La mayoría delas veces el texto procesado es importado a un *software*
de diagramación para la producción de una **publicación impresa**. La ventaja
principal es la posibilidad de visualmente componer el texto. Pero su desventaja
reside en que se complica la **publicación multiformato** al ser muy difícil
regresar a un formato adecuado para la publicación digital.

```
Formato | Tipo    | Programa usual | Licencia
INDD    | Cerrado | InDesign       | Propietaria
SLA     | Abierto | Scribus        | Libre
```

### Para imágenes

El tratamiento con las imágenes es algo común en la labor editorial,
los formatos más comunes tenemos:

```
Formato | Tipo        | Programa usual         | Licencia
JPEG    | Abierto     | Photoshop / Gimp       | Propietaria / Libre
PNG     | Abierto     | Photoshop / Gimp       | Propietaria / Libre
TIFF    | Abierto     | Photoshop / Gimp       | Propietaria / Libre
PS      | Abierto     | Photoshop / Gimp       | Propietaria / Libre
SVG     | Abierto     | Illustrator / Inkscape | Propietaria / Libre
AI      | Propietario | Illustrator            | Propietaria
PSD     | Propietario | Photoshop              | Propietaria
```

## Para libros electrónicos estándar



### Para cómic

Por último, se cuenta con un formato específico para la lectura de
cómic que son: [CBR]{.versalita}, [CBZ]{.versalita}, [CB7]{.versalita} o 
[CBT]{.versalita}.

```
Formato | Tipo    | Programa usual | Licencia
CBR     | Abierto | Renderizador   | Variable
CBZ     | Abierto | Renderizador   | Variable
CB7     | Abierto | Renderizador   | Variable
CBT     | Abierto | Renderizador   | Variable
```

> El último carácter hace mención al formato de compresión, [R]{.versalita} para 
> [RAR]{.versalita}, [Z]{.versalita} para [ZIP]{.versalita}, 7 para 
> [7ZIP]{.versalita} y [T]{.versalita} para [TAR]{.versalita}.

### Renderizadores

Un renderizador es un *software* que permite ver visualmente un
libro electrónico. Puede ser un programa independiente o una extensión a un
explorador *web*. Los renderizadores más populares son:

```
Programa          | Licencia    | Plataforma
Calibre           | Libre       | Multiplataforma
Readium           | Abierto     | Plugin para Chrome
EPUBReader        | ?           | Plugin para Firefox
ADE               | Propietaria | Windows y macOS
iBooks            | Propietaria | iOS y macOS
Sumatra PDF       | Abierto     | Windows
Okular            | Libre       | Linux
Google Play Books | Propietaria | Android
Familia Kindle    | Propietaria | Multiplataforma
```

</section>
